//Se importa Express a traves de una constante que lo importa
const express = require('express');
const app = express();
const PORT = 8080;

//Se usa middleware para manejar el body del JSON
app.use(express.json())

//Nos avisa a traves de consola que el servidor esta encendido
app.listen(PORT, () => {
    console.log(`Estoy vivo a traves de ${PORT}`);
})

//GET que suma los numeros introducidos en la ruta
app.get('/results/:n1/:n2', (req, res) => {
    //Se obtienen los parametros de la ruta y se suman almacenandose
    //en una variable
    const n1 = parseInt(req.params.n1);
    const n2 = parseInt(req.params.n2);
    const result = n1 + n2;
    //En caso de que se haga la solicitud correcta se manda el codigo 200
    //y el resultado
    res.status(200).send(result.toString());
});

//POST que multiplica los numeros introducidos en el body JSON
app.post('/results', (req, res) => {
    //Aqui se almacenan los valores del JSON en variables
    const { n1 } = parseInt(req.body.n1);
    const { n2 } = parseInt(req.body.n2);
    //Se hace la multiplicacon y se almacena en una variable
    const result = n1 * n2;
    console.log(n1);
    //Se comprueba que hay n1 y n2 recibidos del json
    if(!n1){
        res.status(418).send({message: 'Se necesitan ambos numeros (falta n1)'});
    }else if(!n2){
        res.status(418).send({message: 'Se necesitan ambos numeros (falta n2)'})
    }
    //Manda el resultado de la operacion si hay ambos parametros
    res.send({
        Resultados: `El resultado es: ${result}`,
    });
})

app.put('/results', (req, res) => {
    //Aqui se almacenan los valores del JSON en variables
    const { n1 } = parseInt(req.body.n1);
    const { n2 } = parseInt(req.body.n2);
    //Se hace la division y se almacena en una variable
    const result = n1 / n2;

    //Se comprueba que hay n1 y n2 recibidos del json
    if(!n1){
        res.status(418).send({message: 'Se necesitan ambos numeros (falta n1)'});
    }else if(!n2){
        res.status(418).send({message: 'Se necesitan ambos numeros (falta n2)'})
    }
    //Manda el resultado de la operacion si hay ambos parametros
    res.send({
        Resultados: `El resultado es: ${result}`,
    });
})

app.patch('/results', (req, res) => {
    //Aqui se almacenan los valores del JSON en variables
    const { n1 } = parseInt(req.body.n1);
    const { n2 } = parseInt(req.body.n2);
    //Se hace la operacion y se almacena en una variable
    const result = Math.pow(n1, n2);

    //Se comprueba que hay n1 y n2 recibidos del json
    if(!n1){
        res.status(418).send({message: 'Se necesitan ambos numeros (falta n1)'});
    }else if(!n2){
        res.status(418).send({message: 'Se necesitan ambos numeros (falta n2)'})
    }
    //Manda el resultado de la operacion si hay ambos parametros
    res.send({
        Resultados: `El resultado es: ${result}`,
    });
})

app.delete('/results/:n1/:n2', (req, res) => {
    //Se obtienen los parametros de la ruta y se suman almacenandose
    //en una variable
    const n1 = parseInt(req.params.n1);
    const n2 = parseInt(req.params.n2);
    const result = n1 - n2;
    //En caso de que se haga la solicitud correcta se manda el codigo 200
    //y el resultado
    res.status(200).send(result.toString());
})
